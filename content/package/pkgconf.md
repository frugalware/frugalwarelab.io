+++
draft = false
title = "pkgconf 2.4.0-1"
version = "2.4.0-1"
description = "A system for managing library compile/link flags"
date = "2025-03-06T08:09:10"
aliases = "/packages/222559"
categories = ['devel']
upstreamurl = "https://gitea.treehouse.systems/ariadne/pkgconf"
arch = "x86_64"
size = "66884"
usize = "207229"
sha1sum = "bd92866bfff85235c182c037561b98694dfc28d7"
depends = "['glibc']"
+++
### Description: 
A system for managing library compile/link flags

### Files: 
* /usr/bin/i686-pc-linux-gnu-pkg-config
* /usr/bin/pkg-config
* /usr/bin/pkgconf
* /usr/bin/x86_64-pc-linux-gnu-pkg-config
* /usr/include/pkgconf/libpkgconf/bsdstubs.h
* /usr/include/pkgconf/libpkgconf/iter.h
* /usr/include/pkgconf/libpkgconf/libpkgconf-api.h
* /usr/include/pkgconf/libpkgconf/libpkgconf.h
* /usr/include/pkgconf/libpkgconf/stdinc.h
* /usr/lib/libpkgconf.so
* /usr/lib/libpkgconf.so.6
* /usr/lib/libpkgconf.so.6.0.0
* /usr/lib/pkgconfig/libpkgconf.pc
* /usr/share/aclocal/pkg.m4
* /usr/share/doc/pkgconf-2.4.0/AUTHORS
* /usr/share/doc/pkgconf-2.4.0/COPYING
* /usr/share/doc/pkgconf-2.4.0/NEWS
* /usr/share/doc/pkgconf-2.4.0/README.md
* /usr/share/doc/pkgconf/AUTHORS
* /usr/share/doc/pkgconf/README.md
* /usr/share/man/man1/pkgconf.1.gz
* /usr/share/man/man5/pc.5.gz
* /usr/share/man/man5/pkgconf-personality.5.gz
* /usr/share/man/man7/pkg.m4.7.gz
* /usr/share/pkgconfig/personality.d/i686-pc-linux-gnu.personality
* /usr/share/pkgconfig/personality.d/x86_64-pc-linux-gnu.personality
